using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public ObjectPool objectPool;

    public GameObject coinPrefab;
    public GameObject pilarPrefab;
    public GameObject multiPrefab;
    public GameObject shieldPrefab;
    public GameObject enemyChaserPrefab;
    public GameObject HpPrefab;

    public Button restartButton;
    public TextMeshProUGUI gameOverTxt;
    //private Player_Controller pc;
    public bool gameOver;


    // Start is called before the first frame update
    void Start()
    {
        gameOver = false;
        gameOverTxt.enabled = false;
        restartButton.gameObject.SetActive(false);
        Time.timeScale = 1f;

        objectPool.InitializePool(coinPrefab, 20);
        objectPool.InitializePool(pilarPrefab, 2);
        objectPool.InitializePool(multiPrefab, 5);
        objectPool.InitializePool(shieldPrefab, 5);
        objectPool.InitializePool(enemyChaserPrefab, 7);
        objectPool.InitializePool(HpPrefab, 3);
    }

    void Update()
    {
        if (gameOver)
        {
            gameOverTxt.enabled = true;
            restartButton.gameObject.SetActive(true);
            Time.timeScale = 0f;
        }
    }

    void SpawnObject(GameObject prefab)
    {
        GameObject obj = objectPool.GetPooledObject(prefab);

        if(obj != null)
        {
            obj.transform.position = Vector3.zero;
            obj.SetActive(false);
        }
    }

    public void restartScene()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;

        SceneManager.LoadScene(currentSceneIndex);
    }
}
