using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyChaser_Damage : MonoBehaviour
{
    public int damage = 1;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Player_Controller pc = other.GetComponent<Player_Controller>();

            if(pc != null)
            {
                pc.ApplyDamage(damage);
            }

            gameObject.SetActive(false);

        }
    }
}
