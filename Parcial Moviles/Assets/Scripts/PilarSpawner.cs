using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PilarSpawner : MonoBehaviour
{
    public ObjectPool objectPool;
    public float spawnRate = 0.5f;
    public float spawnX = 0f;
    public float spawnY = 6f;
    public GameObject prefab;
    private float nextSpawnTime;

    // Update is called once per frame
    void Update()
    {
        if (Time.time >= nextSpawnTime)
        {
            SpawnPilar();
            nextSpawnTime = Time.time + 1f / spawnRate;
        }
    }

    void SpawnPilar()
    {
        GameObject pilar = objectPool.GetPooledObject(prefab);

        if (pilar != null)
        {
            //float spawnX = Random.Range(-spawnXRange, spawnXRange);
            pilar.transform.position = new Vector2(spawnX, spawnY);
            pilar.SetActive(true);
        }
    }
}
