using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthUI : MonoBehaviour
{
    public List<RawImage> hearts;

    public void UpdateHearts(int currentHp, int maxHp)
    {
        for (int i = 0; i < hearts.Count; i++)
        {
            if (i < currentHp)
            {
                hearts[i].gameObject.SetActive(true);
            }
            else
            {
                hearts[i].gameObject.SetActive(false);
            }
        }
    }
}
