using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin_Controller : MonoBehaviour
{
    public int coinValue = 1;
    public float coinSpeed = 2f;

    private Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(0, -coinSpeed);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Player_Controller pc = other.GetComponent<Player_Controller>();

            if(pc != null)
            {
                pc.AddScore(coinValue);

            }

            gameObject.SetActive(false);
        }
        else if (other.CompareTag("Wall"))
        {
            gameObject.SetActive(false);
        }
    }
}
