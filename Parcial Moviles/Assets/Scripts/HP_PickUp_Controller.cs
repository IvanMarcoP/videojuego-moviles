using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HP_PickUp_Controller : MonoBehaviour
{
    public float fallSpeed = 2f;
    private int hpIncrease = 1;

    // Update is called once per frame
    void Update()
    {
        FallDown();
    }

    void FallDown()
    {
        transform.position += Vector3.down * fallSpeed * Time.deltaTime;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Player_Controller pc = other.GetComponent<Player_Controller>();

            if(pc != null)
            {
                pc.HealPLayer(hpIncrease);

            }

            gameObject.SetActive(false);
        }
        else if (other.CompareTag("Out"))
        {
            gameObject.SetActive(false);
        }
    }
}
