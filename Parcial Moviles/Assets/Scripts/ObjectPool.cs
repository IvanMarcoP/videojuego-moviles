using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    // Diccionario para almacenar los pools de objetos por tipo
    private Dictionary<string, List<GameObject>> pools = new Dictionary<string, List<GameObject>>();

    // M�todo para inicializar un pool para un tipo de objeto
    public void InitializePool(GameObject prefab, int poolSize)
    {
        // Verificar si ya existe un pool para este tipo de objeto
        string key = prefab.name;
        if (!pools.ContainsKey(key))
        {
            // Crear un nuevo pool
            List<GameObject> objectPool = new List<GameObject>();
            for (int i = 0; i < poolSize; i++)
            {
                GameObject obj = Instantiate(prefab, transform);
                obj.SetActive(false);
                objectPool.Add(obj);
            }
            pools.Add(key, objectPool);
        }
    }

    // M�todo para obtener un objeto del pool seg�n su tipo
    public GameObject GetPooledObject(GameObject prefab)
    {
        string key = prefab.name;
        if (pools.ContainsKey(key))
        {
            // Buscar un objeto inactivo en el pool
            foreach (GameObject obj in pools[key])
            {
                if (!obj.activeInHierarchy)
                {
                    return obj;
                }
            }

            // Si no hay objetos inactivos, agregar uno nuevo al pool
            //GameObject newObj = Instantiate(prefab, transform);
            //newObj.SetActive(false);
            //pools[key].Add(newObj);
            //return newObj;
        }
        else
        {
            // Si el tipo de objeto no tiene un pool, devolver null
            Debug.LogWarning("No se ha inicializado el pool para el objeto: " + key);
            return null;
        }

        return null;
    }
}
        //GameObject newObj = Instantiate(prefab);
        //newObj.SetActive(false);
        //pool.Add(newObj);
        //return newObj;
