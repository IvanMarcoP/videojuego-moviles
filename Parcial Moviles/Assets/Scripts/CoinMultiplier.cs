using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinMultiplier : MonoBehaviour
{
    public int coinMulti = 2;
    public float coinSpeed = 2f;

    private Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(0, -coinSpeed);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            ActivateMultiplier(other.gameObject);
            gameObject.SetActive(false);
        }
        else if (other.CompareTag("Wall"))
        {
            gameObject.SetActive(false);
        }
    }

    void ActivateMultiplier(GameObject player)
    {
        Player_Controller playerController = player.GetComponent<Player_Controller>();

        if (playerController != null)
        {
            playerController.ActivateCoinMulti(coinMulti);
        }
    }
}
