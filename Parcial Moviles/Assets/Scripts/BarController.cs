using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarController : MonoBehaviour
{
    public float xSpeed = 2f;
    public float xAmplitude = 1f;
    public float ySpeed = 2f;
    public float yAmplitude = 1f;
    private float initialX;
    private float initialY;
    private float startTime;

    // Start is called before the first frame update
    void Start()
    {
        initialX = transform.position.x;
        initialY = transform.position.y;

        startTime = Time.time;
    }

    void OnEnable()
    {
        initialX = transform.position.x;
        initialY = transform.position.y;

        startTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        float elapsedTime = Time.time - startTime;

        float newX = initialX + Mathf.Sin(elapsedTime * xSpeed) * xAmplitude;

        float newY = initialY - elapsedTime * ySpeed * yAmplitude;

        transform.position = new Vector3(newX, newY, transform.position.z);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Player_Controller pc = other.GetComponent<Player_Controller>();

            if (pc != null)
            {
                pc.ApplyDamage(1);
            }
        }
        else if (other.CompareTag("Out"))
        {
            gameObject.SetActive(false);
        }
    }
}
