using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinSpawner : MonoBehaviour
{
    public ObjectPool objectPool;
    public float spawnRate = 2f;
    public float spawnXRange = 2f;
    public float spawnY = 6f;
    public GameObject prefab;
    private float nextSpawnTime;

    // Update is called once per frame
    void Update()
    {
        if(Time.time >= nextSpawnTime)
        {
            SpawnCoin();
            nextSpawnTime = Time.time + 1f / spawnRate;
        }
    }

    void SpawnCoin()
    {
        GameObject coin = objectPool.GetPooledObject(prefab);

        if( coin != null )
        {
            float spawnX = Random.Range( -spawnXRange, spawnXRange );
            coin.transform.position = new Vector2(spawnX, spawnY);
            coin.SetActive(true);
        }
    }
}
