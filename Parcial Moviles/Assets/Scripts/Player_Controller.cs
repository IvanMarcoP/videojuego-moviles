using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Player_Controller : MonoBehaviour
{
    private Rigidbody2D rb;

    [Header("Datos Player")]
    public float speed = 10f;
    public float rotationSpeed = 10f;
    public TMP_Text textScore;
    [SerializeField] private int score = 0;

    [Header("Multiplicador")]
    [SerializeField] private int multiplicador;
    [SerializeField] private float multiplicadorEndTime = 0f;
    public float multiplicadorDuration = 10f;
    public TMP_Text x2Txt;
    [SerializeField] private bool multiActive;

    [Header("Shield & Life")]
    public int currentLife;
    [SerializeField] private int maxLifes = 3;
    public GameObject shield;
    [SerializeField] private bool isActiveShield;

    public HealthUI healthUI;
    public GameManager GM;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        currentLife = maxLifes;
        shield.SetActive(false);
        x2Txt.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        Movimiento();
        Rotacion();
        TocarPantalla();
        UpdateText();

        if (multiActive)
        {
            multiplicadorEndTime += Time.deltaTime;
            x2Txt.gameObject.SetActive(true);

            if(multiplicadorEndTime >= multiplicadorDuration)
            {
                DeActivateCoinMulti();
            }
        }
    }

    void Movimiento()
    {
        Vector2 tilt = Input.acceleration;

        Vector2 move = new Vector2(tilt.x, tilt.y) * speed;

        rb.velocity = move;
    }

    void Rotacion()
    {
        if (SystemInfo.supportsGyroscope)
        {
            Quaternion rotation = Input.gyro.attitude;
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotationSpeed * Time.deltaTime);
        }
    }

    void TocarPantalla()
    {
        if(Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            Vector2 touchPosition = Camera.main.ScreenToWorldPoint(touch.position);
            rb.velocity = (touchPosition - rb.position).normalized * speed;
        }
    }

    public void AddScore(int value)
    {
        score += value * multiplicador;
        UpdateText();
    }

    public void ActivateCoinMulti(int multi)
    {
        multiActive = true;
        multiplicador = multi;
    }

    public void DeActivateCoinMulti()
    {
        multiplicador = 1;
        multiplicadorEndTime = 0;
        multiActive = false;
        x2Txt.gameObject.SetActive(false);
    }

    public void ApplyDamage(int damage)
    {
        if (isActiveShield)
        {
            BreakShield();
        }
        else
        {
            currentLife -= damage;

            if(currentLife <= 0)
            {
                GM.gameOver = true;
                Debug.Log("Muerto");
            }
        }

        healthUI.UpdateHearts(currentLife, maxLifes);
    }

    public void HealPLayer(int amount)
    {
        currentLife += amount;

        if(currentLife + amount > maxLifes)
        {
            currentLife = maxLifes;
        }

        healthUI.UpdateHearts(currentLife, maxLifes);
    }

    public void ActivateShield(bool state)
    {
        isActiveShield = state;

        if(shield != null)
        {
            shield.SetActive(state);
        }
    }

    private void BreakShield()
    {
        isActiveShield = false;
        shield.SetActive(false);
    }

    void UpdateText()
    {
        if(textScore != null)
        {
            textScore.text = score.ToString();
        }
    }
}
