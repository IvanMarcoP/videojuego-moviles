using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyChaser_Controller : MonoBehaviour
{
    public float speed = 2f;
    public float chaseDuration = 5f;
    public float fallSpeed = 2f;
    [SerializeField] private bool isChasing;
    private Transform player;
    [SerializeField] private float chaseEndTime;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if(isChasing)
        {
            ChasePlayer();

            chaseEndTime += Time.deltaTime;

            if(chaseEndTime >= chaseDuration )
            {
                isChasing = false;
                chaseEndTime = 0f;
                FallDown();
            }
        }
        else
        {
            FallDown();
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player"))
        {
            isChasing = true;
        }
        else if (other.CompareTag("Out"))
        {
            gameObject.SetActive(false);
        }
    }

    void ChasePlayer()
    {
        Vector2 direction = (player.position - transform.position).normalized;
        transform.position = Vector2.MoveTowards(transform.position, player.position, speed * Time.deltaTime);
    }

    void FallDown()
    {
        transform.position += Vector3.down * fallSpeed * Time.deltaTime;
    }
}
